package ru.itcube64;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class Bot extends TelegramLongPollingBot {
    private final String botName = "Wizerd_test_bot";
    private final String botToken = "6909472898:AAHNdb-sCQMh3EQ8xJ2oHOZBsN5zuq2exAE";
    private final Storage storage;

    public Bot() {
        this.storage = new Storage();
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage() && update.getMessage().hasText()) {
                Message inMess = update.getMessage();
                String chatId = inMess.getChatId().toString();
                String responce = parseMess(inMess.getText());

                SendMessage outMess = new SendMessage();
                outMess.setChatId(chatId);
                outMess.setText(responce);

                execute(outMess);


            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    private String parseMess(String textMess) {
        String responce;

        if (textMess.equals("/start")) {
            responce = "Приветствую, бот знает много цитат. Жми /quote, чтобы" + "получить случайную из них.";
        } else if (textMess.equals("/quote")) {
            responce = storage.getRandQuote();
        } else {
            responce = "Сообщение не разпознано!";
        }

        return responce;
    }
}
