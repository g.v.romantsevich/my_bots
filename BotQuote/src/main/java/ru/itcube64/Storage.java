package ru.itcube64;

import java.util.ArrayList;

public class Storage {
    private final ArrayList<String> quotelist;

    public Storage() {
        this.quotelist = new ArrayList<>();

        this.quotelist.add("Начинать всегда стоит с того, что сеет сомнения.\nБорис Стругацкий");
        this.quotelist.add("80% успеха - это появиться в нужном месте в нужное время. \nВуди Аллен");
        this.quotelist.add("Мы должны признать очевидное: понимают лишь те, кто хочет понять. \nБернар Вербер");
    }

    public String getRandQuote() {
        int randValue = (int) (Math.random() * quotelist.size());

        return quotelist.get(randValue);
    }
}
